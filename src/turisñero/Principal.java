/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turisñero;

import App.CreadorPlan;

/**
 *
 * @author paulo
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ///Crear un objeto de la clase CreadorPlan y pasarle el control
        
        CreadorPlan miCreador= new CreadorPlan();
        
        miCreador.iniciarCreación(); 
        
        System.exit(0);
        
    }
    
}
