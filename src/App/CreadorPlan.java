/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import javax.swing.JFrame;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/**
 *
 * @author paulo
 */
public class CreadorPlan {
    
    ContenedorFormulario miContenedor;
    FormularioPlan miFormulario;
    PlanTurístico miPlanTurístico;
    Database miBaseDatos;

    public CreadorPlan() {
        this.miContenedor=new ContenedorFormulario();
        this.miFormulario=new FormularioPlan(miContenedor,true);
        this.miPlanTurístico=new PlanTurístico();
        this.miBaseDatos=new Database();
    }
    
    public void iniciarCreación(){
        
        this.mostrarFormulario();
        this.armarPlan();
        this.crearPlan();
    }
    
    
    private void mostrarFormulario(){
        
        this.miFormulario.setVisible(true);
    }
    
    
    private void armarPlan(){
        //2. Rescatar los datos del formulario y guardarlos en miPlanTurístico
        this.miPlanTurístico.setNombre(this.miFormulario.getNombre());        
        this.miPlanTurístico.setDescripción(this.miFormulario.getDescripción());
    }

    private void crearPlan() {
        //1. Pasarle el objeto miPlan al objto de la base de datos para que lo guarde
        this.miBaseDatos.conectar();
        this.miBaseDatos.escribir(this.miPlanTurístico);
        
    }
}
