/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;


/**
 *
 * @author paulo
 */
public class Database {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    
    public boolean conectar(){
         try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/turisñero?autoReconnect=true&useSSL=false","root","gardel");
        } catch (Exception e) {
             System.out.print(e);
        } 
     return true;   
    }
    
    public boolean escribir(PlanTurístico plan){
        try {
          
            preparedStatement = (PreparedStatement) connect.prepareStatement("INSERT INTO plan_turístico VALUES (default, ?, ?)");
            preparedStatement.setString(1, plan.getNombre());
            preparedStatement.setString(2, plan.getDescripción());
            preparedStatement.executeUpdate();

        } catch (Exception e) {
            System.out.print(e);
        } 
        return true;   
    }
    
    public boolean leer(){
        
     return true;   
    }
    
    
    
    
}
